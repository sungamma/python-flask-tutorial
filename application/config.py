import os
basedir = os.path.abspath(os.path.dirname(__file__))
APP_PATH = os.path.dirname(__file__)


class Config(object):
    MS_TRANSLATOR_KEY = os.environ.get('MS_TRANSLATOR_KEY')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s/database.db?check_same_thread=False' % APP_PATH
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = b'v\x16S~\x87\xf8\xc7@\x9b\x0c(\x1a9mu\xf4'
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = 1
    MAIL_USERNAME = 'nickitest4@gmail.com'
    MAIL_PASSWORD = '!q@w#e$r'
    ADMINS = ['nickitest4@gmail.com']
    POSTS_PER_PAGE = 10
    PORT = 8080
    LANGUAGES = ['en', 'uk', 'ru']


