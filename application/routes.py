from flask import render_template, flash, redirect, request, url_for, jsonify
from flask_login import login_user, login_required, current_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.urls import url_parse
from datetime import datetime
from guess_language import guess_language
from flask import g
from flask_babel import get_locale
from flask_mail import Message

from application import app, models, db, login_manager, mail, Config
from .forms import LoginForm, RegForm, EditProfileForm, PostForm
from .translate import translate as api_translate


@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(user_id)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    # user = {'username': models.User.query.filter_by(username="Pashtet").first().username}
    msg = Message("Hello alshdajk;sgdasd",
                  sender=Config.ADMINS[0],
                  recipients=[Config.ADMINS[0]])
    mail.send(msg)
    form = PostForm()
    if form.validate_on_submit():
        language = guess_language(form.post.data)
        if language == 'UNKNOWN' or len(language) > 5:
            language = ''
        post = models.Post(body=form.post.data, author=current_user, language=language)
        db.session.add(post)
        db.session.commit()
        flash('You have created a new post!')
        return redirect('index')
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('index', page=posts.next_num) if posts.next_num else None
    prev_url = url_for('index', page=posts.prev_num) if posts.has_prev else None
    return render_template('index.html', title="Main page", posts=posts.items, form=form, next_url=next_url, prev_url=prev_url)


@app.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = models.Post.query.order_by(models.Post.timestamp.desc()).paginate(page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('explore', page=posts.next_num) if posts.next_num else None
    prev_url = url_for('explore', page=posts.prev_num) if posts.has_prev else None
    return render_template("index.html", title='Explore', posts=posts.items, next_url=next_url, prev_url=prev_url)


@app.route('/translate', methods=['POST'])
@login_required
def translate():
    return jsonify({'text': api_translate(request.form['text'],
                                      request.form['source_lang'],
                                      request.form['dest_lang'])})

@app.route('/secret_page', methods=["GET"])
@login_required
def secret_page():
    return "this is SECRET JAJAJAA"


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/index')
    form = LoginForm()
    if form.validate_on_submit():
        username = form.data.get("username")
        password = form.data.get("password")
        user = models.User.query.filter_by(username=username).first() or None
        if user and check_password_hash(user.password, password):
            user.authenticated = True

            db.session.add(user)
            db.session.commit()
            login_user(user)

            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_parse('index')
            return redirect(next_page)
        else:
            flash('Invalid username or password')
            return redirect('/login')
    return render_template('login.html', title='Log In', form=form)


@app.route('/reg', methods=['GET', 'POST'])
def reg():
    if current_user.is_authenticated:
        return redirect('/index')
    form = RegForm()
    if form.validate_on_submit():
        username = form.data.get("username")
        password = form.data.get("password")
        email = form.data.get("email")
        hashed_pwd = generate_password_hash(password)

        print("We got a new user here! ", username, password, email)
        new_user = models.User(username=username, password=hashed_pwd, email=email, authenticated=True)

        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)
        return redirect('/index')
    return render_template('reg.html', title='Sign In', form=form)


@app.route('/logout')
@login_required
def logout():
    current_user.authenticated = False
    db.session.add(current_user)
    db.session.commit()
    logout_user()
    return redirect('/index')


@app.route('/user/<username>')
@login_required
def user(username):
    user = models.User.query.filter_by(username=username).first_or_404()
    posts = user.posts
    return render_template('user.html', user=user, posts=posts)


@app.route('/follow/<username>')
@login_required
def follow(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.follow(user)
    db.session.commit()
    flash(f'You are following {username}!')
    return redirect(url_for('user', username=username))


@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.unfollow(user)
    db.session.commit()
    flash(f'You are not following {username}!')
    return redirect(url_for('user', username=username))


@app.before_request
def before_request():
    if current_user.is_authenticated:
        g.locale = str(get_locale())
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
        return render_template('edit_profile.html', title='Edit Profile', form=form)
