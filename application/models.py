from application import db
import datetime
from hashlib import md5


ROLE_USER = 0
ROLE_ADMIN = 1

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)


class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), unique = True)
    password = db.Column(db.String(120))
    email = db.Column(db.String(120))
    authenticated = db.Column(db.Boolean(), default = False)
    role = db.Column(db.SmallInteger, default = ROLE_USER)
    about_me = db.Column(db.String(150))
    last_seen = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    posts = db.relationship('Post', backref = 'author', lazy = 'select')
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='select'), lazy='select'
    )

    def __repr__(self):
        return '<User %r>' % (self.username)

    def __init__(self, username, password, email, authenticated=False, role=''):
        self.username = username
        self.password = password
        self.email = email
        self.authenticated = authenticated
        self.role = role

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.count(user) > 0

    def count_followers(self):
        return len(self.followers)

    def count_followed(self):
        return len(self.followed)

    def followed_posts(self):
        return Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
            followers.c.follower_id == self.id).order_by(
            Post.timestamp.desc())


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(500))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    language = db.Column(db.String(5))

    def __repr__(self):
        return '<Post %r>' % (self.body)

    def __init__(self, body, author, language):
        self.body = body
        self.author = author
        self.language = language
        self.timestamp = datetime.datetime.utcnow()
